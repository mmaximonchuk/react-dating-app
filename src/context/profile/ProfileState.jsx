import React, { useReducer } from 'react';
import { ProfileContext } from './profileContext';
import { profileReducer } from './profileReducer';
import { SHOW_LOADER, SET_PROFILE, HIDE_LOADER, SET_COUNTRIES, SET_PHOTO } from '../types';
import {
  requestCountries,
  requestLoginAuth,
  requestPhotoUpload,
  requestProfileData,
} from '../../api/profileAPI';
function ProfileState({ children }) {
  const initialState = {
    personData: {},
    countries: [],
    // photos: [],
    errMsg: '',
    isLoading: false,
  };
  const [state, dispatch] = useReducer(profileReducer, initialState);

  const showLoader = () => dispatch({ type: SHOW_LOADER });
  const hideLoader = () => dispatch({ type: HIDE_LOADER });

  const setProfileData = async () => {
    showLoader();

    localStorage.removeItem('token');
    let login = localStorage.getItem('l');
    let password = localStorage.getItem('p');

    let result = await requestLoginAuth(login, password);

    localStorage.setItem('token', result.token);
    let payload = await requestProfileData();
    console.log(payload);
    if (payload?.errMsg?.length) {
      dispatch({ type: SET_PROFILE, payload });
    } else {
      dispatch({ type: SET_PROFILE, payload });
      hideLoader();
    }
  };

  const requestCheckCredentials = async (login, password) => {
    let result = await requestLoginAuth(login, password);
    // console.log('requestCheckCredentials:', result);
    return result;
  };

  const getCountries = async () => {
    let { result } = await requestCountries();
    let countries = result.countries;
    dispatch({ type: SET_COUNTRIES, countries });
  };
  const uploadPhoto = async (photoObj) => {
    let result = await requestPhotoUpload(photoObj);
    // dispatch({ type: SET_PHOTO, result });

    return result;
    // let photo = result;
  };
  return (
    <ProfileContext.Provider
      value={{
        showLoader,
        setProfileData,
        getCountries,
        requestCheckCredentials,
        uploadPhoto,
        personData: state.personData,
        isLoading: state.isLoading,
        errMsg: state.errMsg,
        requiredCountries: state.countries,
      }}
    >
      {children}
    </ProfileContext.Provider>
  );
}

export default ProfileState;
