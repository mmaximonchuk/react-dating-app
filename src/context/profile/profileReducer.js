import {SET_PROFILE, SET_COUNTRIES,SET_PHOTO, SHOW_LOADER, HIDE_LOADER} from '../types';

const handlers = {
    [SHOW_LOADER]: (state) => ({...state, isLoading: true}),
    [HIDE_LOADER]: (state) => ({...state, isLoading: false}),
    [SET_COUNTRIES]: (state, action) => ({...state, ...action}),
    [SET_PHOTO]: (state, action) => ({...state, ...action}),
    [SET_PROFILE]: (state, {payload}) => {
        // console.log("payload:", payload);
        if(payload.errMsg) {
            return {
                errMsg: payload.errMsg
            }
        }
        return { 
            personData: payload
        }
    },
    DEFAULT: state => state
}

export const profileReducer = (state, action) => {
    const handle = handlers[action.type] || handlers.DEFAULT;
    return handle(state, action) 
}