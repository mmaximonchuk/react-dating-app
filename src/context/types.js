export const SET_PROFILE = 'profile/SET_PROFILE';
export const SHOW_LOADER = 'profile/SHOW_LOADER';
export const HIDE_LOADER = 'profile/HIDE_LOADER';
export const SET_COUNTRIES = 'profile/SET_COUNTRIES';
export const SET_PHOTO = 'profile/SET_PHOTO';
