import SignIn from './components/pages/SignIn';
import SignUp from './components/pages/SignUp';
import ManProfile from './components/pages/ManProfile';
import ManProfileEdit from './components/pages/ManProfileEdit';

import {MAN_PROFILE_ROUTE , MAN_PROFILE_EDIT_ROUTE} from './utils/constants';

export const ROUTES = [
    {
        id: 1,
        path: MAN_PROFILE_ROUTE,
        component: ManProfile
    },
    {
        id: 2,
        path: MAN_PROFILE_EDIT_ROUTE,
        component: ManProfileEdit
    }
]