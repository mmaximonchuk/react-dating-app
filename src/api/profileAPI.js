export const requestProfileData = async () => {
  try {
    const result = await fetch(`${process.env.REACT_APP_PROFILE_REQUEST_URL_2}/rest/person/man/me`, {
      headers: { Authorization: localStorage.getItem('token') },
    });
    // console.log('Profile data', result);
    return result.json();
  } catch (e) {
    console.log(e.errMsg);
  }
};

export const requestUpdateProfile = async (formData) => {
  try {
    const result = await fetch(`${process.env.REACT_APP_PROFILE_REQUEST_URL_2}/rest/person/man/me`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: localStorage.getItem('token'),
      },
      body: JSON.stringify(formData),
    });
    console.log('FormData', result);
    return result.json();
  } catch (e) {
    console.log(e);
  }
};

export const requestLoginAuth = async (login = false, password = false) => {
  try {
    const result = await fetch(`${process.env.REACT_APP_PROFILE_REQUEST_URL_2}/rest/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        login,
        password,
        bonusKey: 0,
        mobile: true,
        loginFromEmail: true,
      }),
    });
    return result.json();
  } catch (e) {
    console.log(e);
  }
};

export const requestCountries = async () => {
  try {
    const result = await fetch(
      `${process.env.REACT_APP_PROFILE_REQUEST_URL}/gloriabrides/services/geoAPI`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cmd: 'CMD_GET_COUNTRIES',
        }),
      }
    );
    return result.json();
  } catch (e) {
    throw new Error('Some error occured', e);
  }
};

export const requestPhotoUpload = async (photoObj) => {
  try {
    const result = await fetch(
      `${process.env.REACT_APP_PROFILE_REQUEST_URL_2}/rest/upload/man/photo`,
      {
        method: 'POST',
        headers: {
          // 'Accept': 'application/json',
          // 'Content-Type':  'multipart/form-data',
          Authorization: localStorage.getItem('token'),
        },
        body: photoObj,
      }
    );
    return result.json();
  } catch (e) {
    throw new Error('Some error occured', e);
  }
};

