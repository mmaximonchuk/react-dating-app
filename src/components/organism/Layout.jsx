import React, { useContext, useEffect, useState } from 'react';
import Footer from '../molecules/Footer';
import Header from '../molecules/Header';
import Navbar from '../molecules/Navbar';

import imgLine from '../../assets/images/layout/Line.png';
import imgOl from '../../assets/images/banner/IconOnlineLadies.png';
import imgNl from '../../assets/images/banner/IconNewLadies.png';
import imgT100l from '../../assets/images/banner/IconTop100.png';
import imgLiceChat from '../../assets/images/banner/IconLiveChat.png';

import '../../assets/styles/components/layout.scss';
import ModeItem from '../atoms/ModeItem';
import ProfileState from '../../context/profile/ProfileState';
import { ProfileContext } from '../../context/profile/profileContext';

function Layout({ children }) {
  const { setProfileData, personData, isLoading, errMsg } = useContext(ProfileContext);
  const [isNavbarOpen, setNavbarOpen] = useState(false);
  const [isMenuOpen, setMenuOpen] = useState(false);
  
  useEffect(() => {
    (async () => {
      setProfileData();
    })();
  }, []);

  return (
    <div className="layout">
      <Header
        setNavbarOpen={setNavbarOpen}
        isNavbarOpen={isNavbarOpen}
        isMenuOpen={isMenuOpen}
        setMenuOpen={setMenuOpen}
      />
      <div className="gradient-line">
        <img src={imgLine} alt="imgLine" />
      </div>
      <div className="choose-mode container">
        <div className="row">
          <div className="col-12 col-sm-6 col-lg-3">
            <ModeItem img={imgOl} text="Online ladies" />
          </div>
          <div className="col-12 col-sm-6 col-lg-3">
            <ModeItem img={imgNl} text="Newest ladies" />
          </div>
          <div className="col-12 col-sm-6 col-lg-3">
            <ModeItem img={imgT100l} text="Top 100 Ladies" />
          </div>
          <div className="col-12 col-sm-6 col-lg-3">
            <ModeItem img={imgLiceChat} text="Live Chat" />
          </div>
        </div>
      </div>
      <div className="layout__inner">
        <Navbar />
        <main className={`main`}>{children}</main>
      </div>
      <Footer />
    </div>
  );
}

export default Layout;
