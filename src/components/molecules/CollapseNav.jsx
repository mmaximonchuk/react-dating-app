import React, { useState } from 'react';
import imgArrow from '../../assets/images/navbar/ButtonExpand-Minimize.png'


function CollapseNav({img, title, children}) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={`collapse ${isOpen ? 'collapse--open' : ''}`}>
      <div className="trigger" onClick={() => setIsOpen(!isOpen)}>
        <img className="trigger__icon" src={img} alt={title} />
        <span>{title}</span>
        <img className="trigger__state" src={imgArrow} alt="imgArrow" />
      </div>
      <div className="content">
        {children}
      </div>
      <div className="bottom" />
    </div>
  );
}

export default CollapseNav;
