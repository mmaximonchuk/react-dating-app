import React, { useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { ProfileContext } from '../../context/profile/profileContext';
import Button from '../atoms/Button';

import useResizeScreen from '../../hooks/useResizeScreen';

import CollapseNav from './CollapseNav';
import CollapseLink from '../atoms/CollapseLink';
import ModeItem from '../atoms/ModeItem';

import imgLogo from '../../assets/images/layout/Лого.png';
import imgUser from '../../assets/images/layout/Avatar.png';
import imgSettings from '../../assets/images/layout/Settings.png';
import imgLogOut from '../../assets/images/layout/LoginOut.png';

import imgMail from '../../assets/images/navbar/iconmail.png';
import imgLadies from '../../assets/images/navbar/IconLadies.png';
import imgFeaturedServices from '../../assets/images/navbar/IconServices.png';
import imgVgifts from '../../assets/images/navbar/IconGifts.png';
import imgFinance from '../../assets/images/navbar/Iconfinance.png';
import imgSearchL from '../../assets/images/navbar/IconSearch.png';
import imgInbox from '../../assets/images/navbar/mail/IconInbox.png';
import imgOutbox from '../../assets/images/navbar/mail/IconOutbox.png';
import imgAdministration from '../../assets/images/navbar/mail/IconAdmin.png';
import imgSpam from '../../assets/images/navbar/mail/IconSpam.png';
import imgTrash from '../../assets/images/navbar/mail/IconTrash.png';
import imgWriteNewLetter from '../../assets/images/navbar/mail/IconWritenewletter.png';
import imgCreateNewFolder from '../../assets/images/navbar/mail/IconCreatenewfolder.png';
import imgPoint from '../../assets/images/navbar/Point.png';
import imgMenu from '../../assets/images/Онлайн.png';
import imgClose from '../../assets/images/layout/close.png';

import { MAN_PROFILE_ROUTE } from '../../utils/constants';
import '../../assets/styles/components/header.scss';

function Header({ setNavbarOpen, isNavbarOpen, isMenuOpen, setMenuOpen }) {
  const { isMobile } = useResizeScreen();

  const { showLoader, personData, isLoading, errMsg } = useContext(ProfileContext);

  return (
    <header className={`header ${isMobile ? 'header--mobile' : ''}`}>
      <div className="container inner">
        {isMobile && (
          <div className="toggle">
            <button onClick={() => setNavbarOpen(!isNavbarOpen)}>
              <img src={isNavbarOpen ? imgClose : imgMenu} alt="" />
            </button>
          </div>
        )}
        {isNavbarOpen && isMobile ? <HeaderNavBarMenu /> : ''}

        <a href="https://kiev2.dreamonelove.com/">
          <img className="logo" src={imgLogo} alt="Logo" />
        </a>

        <>
          {isMobile ? (
            <HeaderMenu isMenuOpen={isMenuOpen} isMobile={isMobile} personData={personData} />
          ) : (
            <HeaderMenu personData={personData} />
          )}
          {isMobile && (
            <div className="toggle">
              <button onClick={() => setMenuOpen(!isMenuOpen)}>
                <img src={isMenuOpen ? imgClose : imgMenu} alt="" />
              </button>
            </div>
          )}
        </>
      </div>
    </header>
  );
}

export default Header;

const HeaderMenu = ({ personData, isMobile = false, isMenuOpen = false }) => {
  return (
    <div className={`profile ${isMobile && isMenuOpen ? 'profile--mobile' : ''}`}>
      <NavLink to={MAN_PROFILE_ROUTE} className="user">
        <img src={personData?.manGeneralDTO?.photoLnk || imgUser} alt="Avatar" />
        <span>
          {personData?.manGeneralDTO?.firstName || 'Bradley'}
          {''} {personData?.manGeneralDTO?.lastName || 'Pitt'}
        </span>
      </NavLink>
      <Button btnType="bordered">
        <img src={imgSettings} alt="imgSettings" />
        <span>Settings</span>
      </Button>
      <Button
        onClick={() => {
          window.location.href = 'https://kiev2.dreamonelove.com';
          localStorage.removeItem('token');
          localStorage.removeItem('l');
          localStorage.removeItem('p');
        }}
      >
        <span>Login Out</span>
        <img src={imgLogOut} alt="imgLogOut" />
      </Button>
    </div>
  );
};

const HeaderNavBarMenu = () => {
  return (
    <div className="profile profile--mobile sidebar">
      <aside className="navbar">
        <ModeItem img={imgSearchL} text={'Search Ladies'} style={{ marginBottom: '20px' }} />
        <CollapseNav img={imgMail} title="mail">
          <CollapseLink
            img={imgInbox}
            name={'Inbox'}
            notification={{ general: 72, count: 2, message: 'new mail' }}
          />
          <CollapseLink img={imgOutbox} name={'Outbox'} notification={{ general: 0 }} />
          <CollapseLink
            img={imgAdministration}
            name={'Administration'}
            notification={{ general: 0 }}
          />
          <CollapseLink img={imgSpam} name={'Spam'} notification={{ general: 0 }} />
          <CollapseLink img={imgTrash} name={'Trash'} notification={{ general: 0 }} />
          <CollapseLink
            img={imgWriteNewLetter}
            name={'Write new letter'}
            notification={{ general: 0 }}
          />
          <CollapseLink
            img={imgCreateNewFolder}
            name={'Create new folder'}
            notification={{ general: 0 }}
          />
        </CollapseNav>

        <CollapseNav img={imgLadies} title="ladies">
          <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
          <CollapseLink img={imgPoint} name={'My favourite ladies'} />
          <CollapseLink img={imgPoint} name={'My favourite photos'} />
          <CollapseLink img={imgPoint} name={'My favourite videos'} />
          <CollapseLink img={imgPoint} name={'My contact list'} />
        </CollapseNav>

        <CollapseNav img={imgFeaturedServices} title="featured services">
          <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
          <CollapseLink img={imgPoint} name={'My favourite ladies'} />
          <CollapseLink img={imgPoint} name={'My favourite photos'} />
          <CollapseLink img={imgPoint} name={'My favourite videos'} />
          <CollapseLink img={imgPoint} name={'My contact list'} />
        </CollapseNav>
        <CollapseNav img={imgVgifts} title="virtual gifts">
          <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
          <CollapseLink img={imgPoint} name={'My favourite ladies'} />
          <CollapseLink img={imgPoint} name={'My favourite photos'} />
          <CollapseLink img={imgPoint} name={'My favourite videos'} />
          <CollapseLink img={imgPoint} name={'My contact list'} />
        </CollapseNav>
        <CollapseNav img={imgFinance} title="finance">
          <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
          <CollapseLink img={imgPoint} name={'My favourite ladies'} />
          <CollapseLink img={imgPoint} name={'My favourite photos'} />
          <CollapseLink img={imgPoint} name={'My favourite videos'} />
          <CollapseLink img={imgPoint} name={'My contact list'} />
        </CollapseNav>
      </aside>
    </div>
  );
};
