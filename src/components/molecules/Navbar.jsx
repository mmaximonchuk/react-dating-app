import React from 'react';
import { NavLink } from 'react-router-dom';

import CollapseNav from './CollapseNav';
import CollapseLink from '../atoms/CollapseLink';
import ModeItem from '../atoms/ModeItem';

import imgMail from '../../assets/images/navbar/iconmail.png';
import imgLadies from '../../assets/images/navbar/IconLadies.png';
import imgFeaturedServices from '../../assets/images/navbar/IconServices.png';
import imgVgifts from '../../assets/images/navbar/IconGifts.png';
import imgFinance from '../../assets/images/navbar/Iconfinance.png';

import imgSearchL from '../../assets/images/navbar/IconSearch.png'
import imgInbox from '../../assets/images/navbar/mail/IconInbox.png'
import imgOutbox from '../../assets/images/navbar/mail/IconOutbox.png'
import imgAdministration from '../../assets/images/navbar/mail/IconAdmin.png'
import imgSpam from '../../assets/images/navbar/mail/IconSpam.png'
import imgTrash from '../../assets/images/navbar/mail/IconTrash.png'
import imgWriteNewLetter from '../../assets/images/navbar/mail/IconWritenewletter.png'
import imgCreateNewFolder from '../../assets/images/navbar/mail/IconCreatenewfolder.png'
import imgPoint from '../../assets/images/navbar/Point.png'
import useResizeScreen from '../../hooks/useResizeScreen';


function Navbar() {
    const { isMobile } = useResizeScreen();
    if (isMobile) {
      return null;
    }

  return (
    <aside className="navbar">
      <ModeItem img={imgSearchL} text={'Search Ladies'} style={{marginBottom: '20px'}}/>
      <CollapseNav img={imgMail} title='mail'>
      <CollapseLink img={imgInbox} name={'Inbox'} notification={{general: 72, count: 2, message: 'new mail'}} />
        <CollapseLink img={imgOutbox} name={'Outbox'} notification={{general: 0}} />
        <CollapseLink img={imgAdministration} name={'Administration'} notification={{general: 0}} />
        <CollapseLink img={imgSpam} name={'Spam'} notification={{general: 0}} />
        <CollapseLink img={imgTrash} name={'Trash'} notification={{general: 0}} />
        <CollapseLink img={imgWriteNewLetter} name={'Write new letter'} notification={{general: 0}} />
        <CollapseLink img={imgCreateNewFolder} name={'Create new folder'} notification={{general: 0}} />
      </CollapseNav>

      <CollapseNav img={imgLadies} title="ladies">
        <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
        <CollapseLink img={imgPoint} name={'My favourite ladies'} />
        <CollapseLink img={imgPoint} name={'My favourite photos'} />
        <CollapseLink img={imgPoint} name={'My favourite videos'} />
        <CollapseLink img={imgPoint} name={'My contact list'} />
      </CollapseNav>

      <CollapseNav img={imgFeaturedServices} title="featured services">
      <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
        <CollapseLink img={imgPoint} name={'My favourite ladies'} />
        <CollapseLink img={imgPoint} name={'My favourite photos'} />
        <CollapseLink img={imgPoint} name={'My favourite videos'} />
        <CollapseLink img={imgPoint} name={'My contact list'} />
      </CollapseNav>
      <CollapseNav img={imgVgifts} title="virtual gifts">
      <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
        <CollapseLink img={imgPoint} name={'My favourite ladies'} />
        <CollapseLink img={imgPoint} name={'My favourite photos'} />
        <CollapseLink img={imgPoint} name={'My favourite videos'} />
        <CollapseLink img={imgPoint} name={'My contact list'} />
      </CollapseNav>
      <CollapseNav img={imgFinance} title="finance">
      <CollapseLink img={imgPoint} name={'Ladies browser your profile'} />
        <CollapseLink img={imgPoint} name={'My favourite ladies'} />
        <CollapseLink img={imgPoint} name={'My favourite photos'} />
        <CollapseLink img={imgPoint} name={'My favourite videos'} />
        <CollapseLink img={imgPoint} name={'My contact list'} />
      </CollapseNav>
    </aside>
  );
}

export default Navbar;
