import React from "react";
import { NavLink } from "react-router-dom";

import imgFb from "../../assets/images/layout/Facebook.png";
import imgYt from "../../assets/images/layout/Youtube.png";
import imgInst from "../../assets/images/layout/Instagram.png";
import imgTwt from "../../assets/images/layout/Twitter.png";
import imgIn from "../../assets/images/layout/LinkedIN.png";
import imgGoogle from "../../assets/images/layout/Google+.png";

import "../../assets/styles/components/footer.scss";

function Footer() {
  return (
    <div className="footer">
      <div className="inner container">
        <div className="left">
          <span>{new Date().getFullYear()}</span>
          <NavLink className="colored" to="/">
            <span className="red-light">dream</span>
            <span className="torq">one</span>
            <span className="red">love</span>
            .com
          </NavLink>
        </div>

        <div className="center">
          <NavLink to="/">Information</NavLink>
          <NavLink to="/">Contact us</NavLink>
          <NavLink to="/">About us</NavLink>
          <NavLink to="/">Blog</NavLink>
          <NavLink to="/">Information</NavLink>
        </div>

        <div className="socials">
          <a href="#" target="_blank">
            <img src={imgFb} alt="imgFb" />
          </a>
          <a href="#" target="_blank">
            <img src={imgYt} alt="imgYt" />
          </a>
          <a href="#" target="_blank">
            <img src={imgInst} alt="imgInst" />
          </a>
          <a href="#" target="_blank">
            <img src={imgTwt} alt="imgTwt" />
          </a>
          <a href="#" target="_blank">
            <img src={imgIn} alt="imgIn" />
          </a>
          <a href="#" target="_blank">
            <img src={imgGoogle} alt="imgGoogle" />
          </a>
        </div>
      </div>
    </div>
  );
}

export default Footer;
