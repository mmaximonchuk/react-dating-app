import React from 'react';


function ModeItem({img, text, style}) {
  return (
    <button className="mode-item" style={style}>
      <img className="icon" src={img} alt="" />
      <span>{text}</span>
    </button>
  );
}

export default ModeItem;
