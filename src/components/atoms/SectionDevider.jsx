import React from 'react';

function SectionDevider({title}) {
  return (
    <div className="section-devider">
        <div className="line"></div>
        <div className="section__title">
            {title}
        </div>
        <div className="line"></div>
      </div>
  );
}

export default SectionDevider;
