import React, { useState, useEffect } from 'react';

function Input({ error, className, callBack = () => {}, ...restProps }) {
  const [errorText, setError] = useState(error);
  useEffect(() => {
    setError(error);
  }, [error]);

  useEffect(() => {
    setError(error);
  }, []);

  const handleErrorVanish = () => {
    setTimeout(() => {
      setError('');
      setTimeout(() => callBack(), 1500);
    }, 1500);
  };

  return (
    <>
      <input
        className={`c-input ${className} ${errorText?.length ? 'active' : ''}`}
        onFocus={handleErrorVanish}
        {...restProps}
      />
      <div className={`error ${errorText?.length ? 'error--visible' : 'error--hidden'}`}>
        <span className="error__text">{error}</span>
      </div>
    </>
  );
}

export default Input;
