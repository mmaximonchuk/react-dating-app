import React from "react";

function DetailString({name, data}) {
  return (
    <div className="item">
      <span className="item__name">{name}:</span>
      <span className="item__data">{data}</span>
    </div>
  );
}

export default DetailString;
