import React from "react";
import { NavLink } from "react-router-dom";

function CollapseLink({
  img,
  name,
  notification: { general = "", count = 0, message = "" } = '',
}) {
  return (
    <NavLink className="collapse-link" to="/">
      <img className="link__item" src={img} alt="img" />
      <p className="link__name">{name}</p>
      {general.length ? <span className="link__general">({general})</span> : ""}
      {count || message ? (
        <span className="link__notification">
          <span className="red">+{count}</span> {message || ""}
        </span>
      ) : (
        ""
      )}
    </NavLink>
  );
}

export default CollapseLink;
