import React, { useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { requestLoginAuth } from '../api/profileAPI';
import ProfileState from '../context/profile/ProfileState';
import { ROUTES } from '../routes';
import { MAN_PROFILE_ROUTE, MAN_PROFILE_EDIT_ROUTE } from '../utils/constants';

function AuthRouter() {

  return (
    <>
      {ROUTES.map((route) => {
        return <Route key={route.id} path={route.path} component={route.component} exact/>;
      })}
      {/* <Redirect to={MAN_PROFILE_ROUTE} /> */}
    </>
  );
}

export default AuthRouter;
