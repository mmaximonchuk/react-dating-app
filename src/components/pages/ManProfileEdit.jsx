import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import Select from 'react-select';

import { ProfileContext } from '../../context/profile/profileContext';
import { requestProfileData, requestUpdateProfile } from '../../api/profileAPI';
import { MAN_PROFILE_ROUTE } from '../../utils/constants';
import { authSelectOptions } from '../../utils/authSelectOptions';
import SectionDevider from '../atoms/SectionDevider';
import Button from '../atoms/Button';
import Input from '../atoms/Input';

import img1 from '../../assets/images/pages/profile/pic1.png';
import img2 from '../../assets/images/pages/profile/pic2.png';
import img3 from '../../assets/images/pages/profile/pic3.png';
import img4 from '../../assets/images/pages/profile/pic4.png';
import img5 from '../../assets/images/pages/profile/pic5.png';
import img6 from '../../assets/images/pages/profile/pic6.png';
import img7 from '../../assets/images/pages/profile/pic7.png';
import imgV1 from '../../assets/images/pages/profile/v1.png';
import imgV2 from '../../assets/images/pages/profile/v2.png';
import imgV3 from '../../assets/images/pages/profile/v3.png';
import imgV4 from '../../assets/images/pages/profile/v4.png';
import imgTemplate from '../../assets/images/pages/profile/template.png';
import imgRemove from '../../assets/images/pages/profile/remove.png';

import '../../assets/styles/pages/man-profile-edit.scss';
import { colourStyles } from '../pages/SignUp';

const photoset = [
  {
    id: 1,
    img: img1,
    isChoosen: true,
  },
  {
    id: 2,
    img: img2,
    isChoosen: false,
  },
  {
    id: 3,
    img: img3,
    isChoosen: false,
  },
  {
    id: 4,
    img: img4,
    isChoosen: false,
  },
  {
    id: 5,
    img: img5,
    isChoosen: false,
  },
  {
    id: 6,
    img: img6,
    isChoosen: false,
  },
  {
    id: 7,
    img: img7,
    isChoosen: false,
  },
];

const videoset = [
  { id: 1, src: imgV1 },
  { id: 2, src: imgV2 },
  { id: 3, src: imgV3 },
  { id: 4, src: imgV4 },
];

const monthsNumber = [];
const days = [];
const years = [];
const haircolor = [];
const eyescolor = [];
const drinking = [];
const smoking = [];
const educations = [];
const lookingForAgeAt = [];
const lookingForAgeTo = [];

Object.keys(authSelectOptions).forEach((option) => {
  for (let i = 0; i < authSelectOptions[option].length; i++) {
    if (option === 'monthsNumber') {
      monthsNumber.push(authSelectOptions[option][i]);
    }
    if (option === 'days') {
      days.push(authSelectOptions[option][i]);
    }
    if (option === 'years') {
      years.push(authSelectOptions[option][i]);
    }
    if (option === 'hairColor') {
      haircolor.push(authSelectOptions[option][i]);
    }
    if (option === 'eyesColor') {
      eyescolor.push(authSelectOptions[option][i]);
    }
    if (option === 'drinking') {
      drinking.push(authSelectOptions[option][i]);
    }
    if (option === 'smoking') {
      smoking.push(authSelectOptions[option][i]);
    }
    if (option === 'education') {
      educations.push(authSelectOptions[option][i]);
    }
    if (option === 'lookingForAgeAt') {
      lookingForAgeAt.push(authSelectOptions[option][i]);
    }
    if (option === 'lookingForAgeTo') {
      lookingForAgeTo.push(authSelectOptions[option][i]);
    }
  }
});

function ManProfileEdit() {
  const history = useHistory();
  const {
    setProfileData,
    requestCheckCredentials,
    isLoading,
    personData,
    errMsg,
    getCountries,
    requiredCountries,
    uploadPhoto,
  } = useContext(ProfileContext);
  // const [formData, setFormData] = useState({});

  const [fieldError, setFieldError] = useState({ firstName: '', lastName: '', password: '' });
  const [countries, setCountries] = useState([]);
  const [personId, setPersonId] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [streetAdress, setStreetAdress] = useState('');

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [countryId, setCountryId] = useState(null);
  const [country, setCountry] = useState('');
  const [heightType, setHeightType] = useState('CM');
  const [weightType, setWeightType] = useState('KG');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [heightFt, setHeightFt] = useState('');
  const [heightCm, setHeightCm] = useState('');
  const [weightKg, setWeightKg] = useState('');
  const [weightPnd, setWeightPnd] = useState('');
  const [hairColor, setHairColor] = useState('');
  const [eyeColor, setEyeColor] = useState(null);
  const [maritalStatus, setMaritalStatus] = useState(null);
  const [religion, setReligion] = useState(null);
  const [education, setEducation] = useState(null);
  const [smoker, setSmoker] = useState(null);
  const [drinker, setDrinker] = useState(null);
  const [nativeLang, setNativeLang] = useState('');
  const [secondLang, setSecondLang] = useState('');
  const [job, setJob] = useState('');
  const [childrenCount, setChildrenCount] = useState('');
  const [childrenSex, setChildrenSex] = useState('');
  const [childrenAge, setChildrenAge] = useState('');
  const [character, setCharacter] = useState('');
  const [hobbies, setHobbies] = useState('');
  const [lookingFor, setLookingFor] = useState('');
  const [lookingAgeAt, setLookingAgeAt] = useState('');
  const [lookingAgeTo, setLookingAgeTo] = useState('');

  const [birthDay, setBirthDay] = useState('');
  const [birthMonth, setBirthMonth] = useState('');
  const [birthYear, setBirthYear] = useState('');
  const [photosArray, setPhotosArray] = useState([]);
  const [newStoredPhotoIds, setNewStoredPhotoIds] = useState([]);
  const [videosetArray, setVideosetArray] = useState(videoset);
  const [choosenPhotoId, setChoosenPhotoId] = useState('');
  const [image, setImageFile] = useState(null);

  const handlePhotoChoice = (e) => {
    // setChoosenPhotoId(e.target.defaultValue);
  };
  const handleRemovePhoto = (e, itemId) => {
    // setPhotosArray(photosArray.filter((photo) => photo.id != itemId));
  };
  const handleRemoveVideo = (e, itemId) => {
    // setVideosetArray(videosetArray.filter((video) => video.id != itemId));
  };
  const handleFieldError = (fieldName, errText = '') => {
    setFieldError({ ...fieldError, [fieldName]: errText || 'This field can`t be empty' });
  };

  const fileSelectHandler = (e) => {
    const param = e.target.files[0];
    let fileName = '';
    let label = document.querySelector('#photo-label');

    if (e.target?.files && e.target.files?.length > 1) {
      fileName = (e.target.getAttribute('data-multiple-caption') || '').replace(
        '{count}',
        e.target.files.length
      );
    } else fileName = e.target.files[0]?.name;

    if (fileName?.length) {
      label.textContent = fileName;
    }
    let formData = new FormData();

    formData.append('file', param);

    setImageFile(formData);
  };

  const sendPhoto = async (e) => {
    e.preventDefault();
    let result = await uploadPhoto(image);
    let newObj = {...result, isChoosen: false}
    if(newObj?.id || newObj?.mediaName) {
      setNewStoredPhotoIds([...newStoredPhotoIds, Number.parseInt(result?.id)]);
    
      const prevData = photosArray !== undefined ? photosArray : []
      setPhotosArray([...prevData, newObj]);
    }

    // console.log('result: ', result);
  };

  const handleCancel = (event) => {};
  const handleSubmit = async (e) => {
    e.preventDefault();
    const birthday = `${birthDay}.${birthMonth}.${birthYear}`;
    const lookingAge = `${lookingAgeAt}-${lookingAgeTo}`;
    if (!firstName.length) {
      handleFieldError('firstName');
      return;
    }
    if (!lastName.length) {
      handleFieldError('lastName');
      return;
    }
    const formData = {
      birthday,
      lookingAge,
      firstName,
      lastName,
      countryId,
      birthday,
      heightType,
      weightType,
      height,
      weight,
      hairColor,
      eyeColor,
      maritalStatus,
      education,
      smoker,
      drinker,
      nativeLang,
      secondLang,
      job,
      childrenCount,
      character,
      hobbies,
      lookingFor,
      lookingAge,
      religion,
      childrenSex,
      childrenCount,
      mediaInDTO: {
        newStoredPhotoIds,
      }
    };

    const checkCredentials = await requestCheckCredentials(email, password);
    if (checkCredentials?.token?.length) {
      console.log(formData);
      const result = await requestUpdateProfile(formData);
      await setProfileData();
      console.log(result);
      history.push(MAN_PROFILE_ROUTE);
    } else {
      handleFieldError('password', 'Your credentials must match');
    }
  };

  useEffect(() => {
    setPersonId(personData?.manGeneralDTO?.id);
    setFirstName(personData?.manGeneralDTO?.firstName);
    setLastName(personData?.manGeneralDTO?.lastName);
    setCountryId(personData?.manGeneralDTO?.countryId);
    setCountry(personData?.manGeneralDTO?.countryName.trim());

    setWeightKg(personData?.manAdditionalDTO?.weightKg);
    setWeightPnd(personData?.manAdditionalDTO?.weightPnd);
    setWeightType(personData?.manAdditionalDTO?.weightKg ? 'KG' : 'LBS');
    setHeightFt(personData?.manAdditionalDTO?.heightFt);
    setHeightCm(personData?.manAdditionalDTO?.heightCm);
    setHeight(
      heightType === 'CM'
        ? personData?.manAdditionalDTO?.heightCm
        : personData?.manAdditionalDTO?.heightFt
    );
    setWeight(
      weightType === 'KG'
        ? personData?.manAdditionalDTO?.weightKg
        : personData?.manAdditionalDTO?.weightPnd
    );
    setHeightType(personData?.manAdditionalDTO?.heightCm ? 'CM' : 'FT');

    setHairColor(personData?.manAdditionalDTO?.hairColor);
    setEyeColor(personData?.manAdditionalDTO?.eyeColor);
    setSmoker(personData?.manAdditionalDTO?.smoker);
    setDrinker(personData?.manAdditionalDTO?.drinker);
    setEducation(personData?.manAdditionalDTO?.education);
    setMaritalStatus(personData?.manAdditionalDTO?.maritalStatus);
    setNativeLang(personData?.manAdditionalDTO?.nativeLanguage);
    setSecondLang(personData?.manAdditionalDTO?.secondLanguage);
    setJob(personData?.manAdditionalDTO?.job);
    setCharacter(personData?.manAdditionalDTO?.character);
    setHobbies(personData?.manAdditionalDTO?.hobbies);
    setLookingFor(personData?.manAdditionalDTO?.lookingFor);
    setLookingAgeAt(personData?.manAdditionalDTO?.lookingAge.split('-')[0]);
    setLookingAgeTo(personData?.manAdditionalDTO?.lookingAge.split('-')[1]);
    setReligion(personData?.manAdditionalDTO?.religion);
    setChildrenCount(personData?.manAdditionalDTO?.childrenCount);
    setChildrenSex(personData?.manAdditionalDTO?.childrenSex);
    setChildrenAge(personData?.manAdditionalDTO?.childrenAge);
    setBirthDay(personData?.manAdditionalDTO?.birthday.split('.')[0]);
    setBirthMonth(personData?.manAdditionalDTO?.birthday.split('.')[1]);
    setBirthYear(
      personData?.manAdditionalDTO?.birthday
        .split('.')[2]
        .split('')
        .slice(0, 4)
        .reduce((acc, curr) => acc + curr)
    );
    setPhotosArray(personData?.manPhotoDTOs);
  }, [personData]);

  useEffect(() => {
    getCountries();
  }, []);

  useEffect(() => {
    if (requiredCountries?.length) {
      setCountries(
        requiredCountries.map((country) => {
          return {
            value: country.id,
            label: country.name.trim(),
          };
        })
      );
    }
  }, [requiredCountries]);

  const showHidePassword = (target) => {
    const input = document.getElementById('password-input');
    if (input.getAttribute('type') === 'password') {
      target.classList.add('view');
      input.setAttribute('type', 'text');
    } else {
      target.classList.remove('view');
      input.setAttribute('type', 'password');
    }
    return false;
  };

  return (
    <div className="man-profile-edit">
        <>
          <h1 className="man-profile-edit__title">My profile</h1>
          <SectionDevider title="My account information" />
          <p className="notation">*This information is visible only to you</p>
          <form onSubmit={handleSubmit}>
            <div className="row my-account-info">
              <div className="col-lg-6 col-12">
                <label className="edit-wrapper">
                  <span>ID:</span>
                  <span className="text-fw-600">{personId}</span>
                </label>
                <label className="edit-wrapper">
                  <span>E-mail*:</span>
                  <input
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    type="text"
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Password*:</span>
                  <Input
                    error={fieldError.password}
                    callBack={() => setFieldError({ ...fieldError, password: '' })}
                    onChange={(e) => setPassword(e.target.value)}
                    className="text-field"
                    type="password"
                    value={password}
                    id="password-input"
                  />
                  <div
                    onClick={(e) => showHidePassword(e.target)}
                    className="password-control"
                  ></div>
                </label>
                <label className="edit-wrapper">
                  <span>Phone number*:</span>
                  <input
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                    type="text"
                    className="text-field"
                  />
                </label>
              </div>
              <div className="col-lg-6 col-12">
                <label className="edit-wrapper">
                  <span>First name*:</span>
                  <Input
                    error={fieldError.firstName}
                    callBack={() => setFieldError({ ...fieldError, firstName: '' })}
                    onChange={(e) => setFirstName(e.target.value)}
                    className="text-field"
                    type="text"
                    value={firstName}
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Last name*:</span>
                  <Input
                    error={fieldError.lastName}
                    callBack={() => setFieldError({ ...fieldError, lastName: '' })}
                    onChange={(e) => setLastName(e.target.value)}
                    className="text-field"
                    type="text"
                    value={lastName}
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Country:</span>
                  <Select
                    name="countryId"
                    onChange={({ value }) => {
                      setCountryId(value);
                      console.log(value);
                    }}
                    className="text-field form-select--country"
                    defaultValue={countries[1]?.value}
                    value={countries.filter((country) => country.value == countryId)}
                    options={countries}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Street adress:</span>
                  <input
                    value={streetAdress}
                    onChange={(e) => setStreetAdress(e.target.value)}
                    type="text"
                    className="text-field"
                  />
                </label>
              </div>
            </div>
            <SectionDevider title="About me" />
            <div className="row about-me">
              <div className="col-lg-6 col-12">
                <label className="edit-wrapper">
                  <span>Country:</span>
                  <Select
                    name="countryId"
                    onChange={({ value }) => {
                      setCountryId(value);
                      console.log(value);
                    }}
                    className="text-field form-select--country"
                    defaultValue={countries[1]?.value}
                    value={countries.filter((country) => country.value == countryId)}
                    options={countries}
                    styles={colourStyles}
                  />
                </label>
                <div className="edit-wrapper">
                  <span>Date of birth:</span>
                  <Select
                    name="monthNumber"
                    onChange={({ value }) => setBirthMonth(value)}
                    className="text-field form-select--month"
                    defaultValue={monthsNumber[1]}
                    options={monthsNumber}
                    styles={colourStyles}
                  />
                  <Select
                    name="day"
                    onChange={({ value }) => setBirthDay(value)}
                    className="text-field form-select--day"
                    defaultValue={days[1]}
                    options={days}
                    styles={colourStyles}
                  />
                  <Select
                    name="year"
                    onChange={({ value }) => setBirthYear(value)}
                    className="text-field form-select--year"
                    defaultValue={years[1]}
                    options={years}
                    styles={colourStyles}
                  />
                </div>
                <div className="edit-wrapper edit-wrapper--spec mt-8">
                  <span>Height:</span>
                  <input
                    onChange={(e) => {
                      setHeight(e.target.value);
                      if (heightType === 'CM') {
                        setHeightCm(e.target.value);
                      } else {
                        setHeightFt(e.target.value);
                      }
                    }}
                    value={heightType === 'CM' ? heightCm : heightFt}
                    type="text"
                    className="text-field text-field--sm"
                  />
                  <input
                    onChange={(e) => setHeightType(e.target.value)}
                    type="radio"
                    id="cm"
                    name="height"
                    value="CM"
                    checked={heightType === 'CM'}
                  />
                  <label for="cm">cm</label>
                </div>
                <div className="edit-wrapper edit-wrapper--spec mt-8">
                  <span>Weight:</span>
                  <input
                    onChange={(e) => {
                      setWeight(e.target.value);
                      if (weightType === 'KG') {
                        setWeightKg(e.target.value);
                      } else {
                        setWeightPnd(e.target.value);
                      }
                    }}
                    value={weightType === 'KG' ? weightKg : weightPnd}
                    type="text"
                    className="text-field text-field--sm"
                  />
                  <input
                    onChange={(e) => setWeightType(e.target.value)}
                    type="radio"
                    id="kg"
                    name="weight"
                    value="KG"
                    checked={weightType === 'KG'}
                  />
                  <label for="kg">kg</label>
                </div>
                <label className="edit-wrapper mt-8">
                  <span>Hair color:</span>
                  <Select
                    name="hairColor"
                    onChange={({ value }) => setHairColor(value)}
                    className="text-field form-select--country"
                    defaultValue={haircolor[1]}
                    options={haircolor}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Maritial status:</span>
                  <input
                    onChange={(e) => setMaritalStatus(e.target.value)}
                    value={maritalStatus}
                    type="text"
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Number of children:</span>
                  <input
                    onChange={(e) => setChildrenCount(e.target.value)}
                    value={childrenCount}
                    type="text"
                    className="text-field"
                  />
                </label>
              </div>
              <div className="col-lg-6 col-12">
                <label className="edit-wrapper mt-8">
                  <span>Eyes color:</span>
                  <Select
                    name="eyesColor"
                    onChange={({ value }) => setEyeColor(value)}
                    className="text-field form-select--country"
                    defaultValue={eyescolor[1]}
                    options={eyescolor}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper mt-8">
                  <span>Drinking:</span>
                  <Select
                    name="drinking"
                    onChange={({ value }) => setDrinker(value)}
                    className="text-field form-select--country"
                    defaultValue={drinking[1]}
                    options={drinking}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper mt-8">
                  <span>Smoking:</span>
                  <Select
                    name="smoking"
                    onChange={({ value }) => setSmoker(value)}
                    className="text-field form-select--country"
                    defaultValue={smoking[1]}
                    options={smoking}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper mt-8">
                  <span>Education:</span>
                  <Select
                    name="education"
                    onChange={({ value }) => setEducation(value)}
                    className="text-field form-select--country"
                    defaultValue={educations[1]}
                    options={educations}
                    styles={colourStyles}
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Native lang:</span>
                  <input
                    onChange={(e) => setNativeLang(e.target.value)}
                    value={nativeLang}
                    type="text"
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Second lang:</span>
                  <input
                    onChange={(e) => setSecondLang(e.target.value)}
                    value={secondLang}
                    type="text"
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper">
                  <span>Job of profession:</span>
                  <input
                    onChange={(e) => setJob(e.target.value)}
                    value={job}
                    type="text"
                    className="text-field"
                  />
                </label>
              </div>
              <div className="col-12 mt-4">
                <label className="edit-wrapper edit-wrapper--lg">
                  <span>Write about yourself:</span>
                  <textarea className="text-field" />
                </label>
                <label className="edit-wrapper edit-wrapper--lg">
                  <span>Character:</span>
                  <textarea
                    onChange={(e) => setCharacter(e.target.value)}
                    value={character}
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper edit-wrapper--lg">
                  <span>Hobbies and interests:</span>
                  <textarea
                    onChange={(e) => setHobbies(e.target.value)}
                    value={hobbies}
                    className="text-field"
                  />
                </label>
                <label className="edit-wrapper edit-wrapper--lg">
                  <span>Looking for a type:</span>
                  <textarea
                    onChange={(e) => setLookingFor(e.target.value)}
                    value={lookingFor}
                    className="text-field"
                  />
                </label>
              </div>
              <div className="col-lg-6 col-12">
                <div className="edit-wrapper">
                  <span>Looking for age:</span>
                  <Select
                    name="monthNumber"
                    onChange={({ value }) => setLookingAgeAt(value)}
                    className="text-field form-select--month"
                    defaultValue={lookingForAgeAt[0]}
                    options={lookingForAgeAt}
                    styles={colourStyles}
                  />
                  <Select
                    name="day"
                    onChange={({ value }) => setLookingAgeTo(value)}
                    className="text-field form-select--day"
                    defaultValue={lookingForAgeTo[0]}
                    options={lookingForAgeTo}
                    styles={colourStyles}
                  />
                </div>
              </div>
              <div className="col-12 mt-4">
                <SectionDevider title="Upload your photos" />
                <div className="photoset photoset--mobile">
                  {photosArray?.map((item) => {
                    // let idsArr = choosenPhoto.map(photo => photo.id)
                    const photoPath = `https://kiev2.dreamonelove.com/gloriabrides/services/test/img/man/${item.mediaName}/photo.jpg`;
                    return (
                      <label key={item.id} className="photoset__item">
                        <img src={photoPath} alt={item.mediaName} />
                        <div className="handlers">
                          <input
                            onChange={(e) => handlePhotoChoice(e)}
                            className="photo-check"
                            type="radio"
                            name="photos"
                            checked={choosenPhotoId == item.id}
                            value={item.id}
                          />
                          <button
                            onClick={(e) => handleRemovePhoto(e, item.id)}
                            className="btn-close"
                          >
                            <img src={imgRemove} alt="imgRemove" />
                          </button>
                        </div>
                      </label>
                    );
                  })}
                </div>
                <div className="row no-gutters selector">
                  <div className="col-lg-10">
                    <input
                      onChange={fileSelectHandler}
                      accept=".png, .svg, .jpeg, .jpg"
                      type="file"
                      name="file"
                      id="photo"
                      className="inputfile"
                    />
                    <label id="photo-label" for="photo">
                      No file selected
                    </label>
                  </div>

                  <div className="col-lg-2">
                    <Button onClick={sendPhoto} className="upload-btn">
                      <span>Upload photo</span>
                    </Button>
                  </div>
                </div>
              </div>
              <div className="col-12 mt-4">
                <SectionDevider title="Upload your videos" />
                <div className="photoset photoset--mobile photoset--lg">
                  {videosetArray.map((item) => {
                    return (
                      <label key={item.id} className="photoset__item">
                        <img src={item.src || imgTemplate} alt="album" />
                        <div className="handlers">
                          <button
                            onClick={(e) => handleRemoveVideo(e, item.id)}
                            className="btn-close"
                          >
                            <img src={imgRemove} alt="imgRemove" />
                          </button>
                        </div>
                      </label>
                    );
                  })}
                </div>
                <div className="row no-gutters selector">
                  <div className="col-lg-10">
                    <input
                      type="file"
                      name="video"
                      id="video"
                      className="inputfile"
                      multiple
                      data-multiple-caption="{count} files selected"
                    />
                    <label for="video">No file selected</label>
                  </div>

                  <div className="col-lg-2">
                    <Button className="upload-btn">
                      <span>Upload video</span>
                    </Button>
                  </div>
                </div>
              </div>
            </div>

            <div className="btn-bottom">
              <Button type="submit">
                <span>save</span>
              </Button>
              <Button onClick={() => history.push(MAN_PROFILE_ROUTE)} btnType="bordered">
                <span>cancel</span>
              </Button>
            </div>
          </form>
        </>
    </div>
  );
}

export default ManProfileEdit;
