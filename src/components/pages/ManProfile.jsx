import React, { useContext, useEffect, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';

import DetailString from '../atoms/DetailString';
import useResizeScreen from '../../hooks/useResizeScreen';
import Loader from '../atoms/Loader';
import SectionDevider from '../atoms/SectionDevider';
import Button from '../atoms/Button';
import { ProfileContext } from '../../context/profile/profileContext';

import imgEdit from '../../assets/images/pages/profile/Edit.png';
import imgUSA from '../../assets/images/pages/profile/usa.png';
import imgAva from '../../assets/images/pages/profile/Img.png';
import imgRodiac from '../../assets/images/pages/profile/virgo.png';
import imgLike from '../../assets/images/pages/profile/heart.png';
import img1 from '../../assets/images/pages/profile/pic1.png';
import img2 from '../../assets/images/pages/profile/pic2.png';
import img3 from '../../assets/images/pages/profile/pic3.png';
import img4 from '../../assets/images/pages/profile/pic4.png';
import img5 from '../../assets/images/pages/profile/pic5.png';
import img6 from '../../assets/images/pages/profile/pic6.png';
import img7 from '../../assets/images/pages/profile/pic7.png';
import imgTemplate from '../../assets/images/pages/profile/template.png';

import '../../assets/styles/pages/man-profile.scss';
import { requestProfileData } from '../../api/profileAPI';
// import { requestLoginAuth } from '../../api/profileAPI';
import { MAN_PROFILE_EDIT_ROUTE } from '../../utils/constants';

function ManProfile() {
  const { isMobile } = useResizeScreen();
  const history = useHistory();
  const [showFullInfo, setShowFullInfo] = useState(false);

  const { setProfileData, personData, isLoading, errMsg } = useContext(ProfileContext);

  // useEffect(() => {
  //   console.log(personData);
  // }, [personData]);

  const toEditPage = () => {
    history.push(MAN_PROFILE_EDIT_ROUTE);
  };

  if (isLoading) {
    return <Loader />;
  }
  return (
    <div className="man-profile">
      <div className="activity">
        <div className={`left ${personData?.manLoginDTO?.online ? 'online' : ''}`}>
          <div className="circle"></div>
          <p className="text">
            {personData?.manLoginDTO?.online
              ? 'Online'
              : `Last visit on the ${personData?.manLoginDTO?.online || '2020.01.07 at 15:20'}`}
          </p>
        </div>

        <div className="right">
          <span className="id">ID: {personData?.manGeneralDTO?.id || '706276'}</span>
          <Button onClick={toEditPage}>
            <img src={imgEdit} alt="imgLogOut" />
            <span>Edit profile</span>
          </Button>
        </div>
      </div>

      <div className="person-info">
        <div className="name">
          <img src={imgUSA} alt="country" />
          <h2 className="person-name">
            {personData?.manGeneralDTO?.firstName || 'Bradley'}{' '}
            {personData?.manGeneralDTO?.lastName || 'Pitt'}
          </h2>
          <h2 className="person-age">{personData?.manGeneralDTO?.age || '56'} Age</h2>
        </div>
        {!isMobile && (
          <div className="details">
            <DetailString
              name="Country"
              data={personData?.manGeneralDTO?.countryName || 'USA, Shawnee, OK'}
            />
            <DetailString
              name="Date of birth"
              data={personData?.manAdditionalDTO?.birthday || '18 December 1963'}
            />
            <DetailString
              name="Height cm/ft"
              data={personData?.manAdditionalDTO?.heightCm || '183cm'}
            />
            <DetailString
              name="Weight kg/lbs"
              data={personData?.manAdditionalDTO?.weightKg || '75kg'}
            />
            <DetailString
              name="Hair color"
              data={personData?.manAdditionalDTO?.hairColor || 'Brown'}
            />
            <DetailString
              name="Eyes color"
              data={personData?.manAdditionalDTO?.eyeColor || 'Blue'}
            />
            <DetailString name="Drinking" data={personData?.manAdditionalDTO?.drinker || 'Never'} />
            <DetailString
              name="Education"
              data={personData?.manAdditionalDTO?.education || 'University degree'}
            />
          </div>
        )}
        <AvatarComponent photoData={personData?.manGeneralDTO} isMobile={isMobile} />
      </div>
      <div className="photoset">
        <div className="photoset__item">
          <img src={img1} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img2} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img3} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img4} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img5} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img6} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={img7} alt="album" />
        </div>
        <div className="photoset__item">
          <img src={imgTemplate} alt="album" />
        </div>
      </div>

      {isMobile && (
        <>
          <SectionDevider title="About me" />
          <div className="details">
            <DetailString
              name="Country"
              data={personData?.manGeneralDTO?.countryName || 'USA, Shawnee, OK'}
            />
            <DetailString
              name="Date of birth"
              data={personData?.manAdditionalDTO?.birthday || '18 December 1963'}
            />
            <DetailString
              name="Height cm/ft"
              data={personData?.manAdditionalDTO?.heightCm || '183cm'}
            />
            <DetailString
              name="Weight kg/lbs"
              data={personData?.manAdditionalDTO?.weightKg || '75kg'}
            />
            <DetailString
              name="Hair color"
              data={personData?.manAdditionalDTO?.hairColor || 'Brown'}
            />
            <DetailString
              name="Eyes color"
              data={personData?.manAdditionalDTO?.eyeColor || 'Blue'}
            />
            <DetailString name="Drinking" data={personData?.manAdditionalDTO?.drinker || 'Never'} />
            <DetailString
              name="Education"
              data={personData?.manAdditionalDTO?.education || 'University degree'}
            />
          </div>
        </>
      )}
      {isMobile ? (
        <div className={`show-more ${showFullInfo ? 'expanded' : ''}`}>
          <Button
            style={{ margin: '30px auto 30px' }}
            onClick={() => setShowFullInfo(!showFullInfo)}
          >
            <span>More information</span>
          </Button>
          <div className="show-more__container">
            <MoreInformationComponent toEditPage={toEditPage} personData={personData} />
          </div>
        </div>
      ) : (
        <MoreInformationComponent toEditPage={toEditPage} personData={personData} />
      )}
    </div>
  );
}

export default ManProfile;

const MoreInformationComponent = ({ personData, toEditPage = () => {} }) => {
  return (
    <>
      <SectionDevider title="More information" />
      <div className="more-information c-block">
        <div className="left">
          <div className="details extended">
            <DetailString name="Native Lang" data={personData?.manAdditionalDTO?.nativeLanguage || "English"} />
            <DetailString name="Second Lang" data={personData?.manAdditionalDTO?.secondLanguage || "Russian"} />
            <DetailString name="Job of Profession" data={personData?.manAdditionalDTO?.job || "Actor"} />
            <DetailString name="Religion" data={personData?.manAdditionalDTO?.religion || "Other"} />
          </div>
        </div>
        <div className="right">
          <div className="details extended">
            <DetailString
              name="Maritial Status"
              data={personData?.manAdditionalDTO?.maritalStatus || "Never married"}
            />
            <DetailString
              name="Number of Childred"
              data={personData?.manAdditionalDTO?.childrenCount || "3"}
            />
            <DetailString name="Children gender" data={personData?.manAdditionalDTO?.childrenSex || "1 Boy, 2 Girl"} />
            <DetailString name="Children age" data={personData?.manAdditionalDTO?.childrenAge || "3, 5, 7"} />
          </div>
        </div>
      </div>
      <div className="c-block">
        <p className="c-title-14">Character:</p>
        <p className="c-text-14">
          {personData?.manAdditionalDTO?.character ||
            'Is an American actor and film producer. He has received multiple awards, including two Golden Globe Awards for his acting, and an Academy Award and a Primetime Emmy Award as producer under his production company, Plan B Entertainment'}
        </p>
      </div>
      <div className="c-block">
        <p className="c-title-14">Hobbies and interests:</p>
        <p className="c-text-14">
          {personData?.manAdditionalDTO?.hobbies ||
            'Is an American actor and film producer. He has received multiple awards, including two Golden Globe Awards for his acting, and an Academy Award and a Primetime Emmy Award as producer under his production company, Plan B Entertainment'}
        </p>
      </div>
      <div className="c-block">
        <p className="c-title-14">Looking for a type:</p>
        <p className="c-text-14">
          {personData?.manAdditionalDTO?.lookingFor ||
            'Is an American actor and film producer. He has received multiple awards, including two Golden Globe Awards for his acting, and an Academy Award and a Primetime Emmy Award as producer under his production company, Plan B Entertainment'}
        </p>
      </div>
      <p className="c-title-14">
        Looking for age: <span>{personData?.manAdditionalDTO?.lookingAge || "18-25"}</span>
      </p>
      <div className="btn-bottom">
        <Button onClick={toEditPage} style={{ marginLeft: 'auto' }}>
          <img src={imgEdit} alt="imgLogOut" />
          <span>Edit profile</span>
        </Button>
      </div>
    </>
  );
};

const AvatarComponent = ({ isMobile = false, photoData = [] }) => {
  return (
    <>
      <div className={`avatar ${isMobile ? 'avatar--mobile' : ''}`}>
        <img src={photoData?.photoLnk || imgAva} alt="avatar" />
        <button className="zodiak">
          <img src={imgRodiac} alt="zodiac" />
        </button>
        <div className="like-count">
          {photoData?.totalVotes !== undefined ? photoData?.totalVotes : '0'}
        </div>
        <button className="like">
          <img src={imgLike} alt="like" />
        </button>
      </div>
      {isMobile && (
        <div className="photoset photoset--mobile">
          <div className="photoset__item">
            <img src={img1} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img2} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img3} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img4} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img5} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img6} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={img7} alt="album" />
          </div>
          <div className="photoset__item">
            <img src={imgTemplate} alt="album" />
          </div>
        </div>
      )}
    </>
  );
};
