import React, { useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import Select from 'react-select';
// import { SIGNIN_ROUTE } from "../../utils/constants";
import { authSelectOptions } from '../../utils/authSelectOptions';

export const colourStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: 'white',
    borderRadius: '20px',
    borderColor: '#959595',
    flexWrap: "nowrap",
  }),
  option: ({ isSelected }) => ({
    padding: '5px 10px',
    color: '#959595',
  }),
  container: (styles) => ({
    // flex: '1 1 40%',
    position: 'relative',
  }),
  menu: () => ({
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    zIndex: '10',
  }),
  input: (styles) => ({ ...styles }),
  placeholder: (styles) => ({ ...styles }),
  singleValue: (styles, { data }) => ({
    ...styles,
    color: '#000A4F',
    fontWeight: '600',
  }),
  valueContainer: (styles, { data }) => ({ ...styles, paddingLeft: '10px' }),
  indicatorSeparator: () => ({ display: 'none' }),
  // indicatorContainer: () => ({ paddingLeft: '0' }),
};

function SignUp() {
  const [formData, setFormData] = useState({});
  const [birthDay, setBirthDay] = useState('');
  const [birthMonth, setBirthMonth] = useState('');
  const [birthYear, setBirthYear] = useState('');
  const history = useHistory();
  const months = [];
  const days = [];
  const years = [];
  const countries = [];

  Object.keys(authSelectOptions).forEach((option) => {
    for (let i = 0; i < authSelectOptions[option].length; i++) {
      if (option === 'months') {
        months.push(authSelectOptions[option][i]);
      }
      if (option === 'days') {
        days.push(authSelectOptions[option][i]);
      }
      if (option === 'years') {
        years.push(authSelectOptions[option][i]);
      }
      if (option === 'countries') {
        countries.push(authSelectOptions[option][i]);
      }
    }
  });

  const handleForms = (e) => {
    const name = e.target?.name;
    const value = e.target?.value;
    if (name === 'noMail') {
      setFormData({ ...formData, [name]: e.target.checked });
    } else {
      setFormData({ ...formData, [name]: value });
    }
    // console.log(formData);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const result = await fetch('https://kiev.dreamonelove.com/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(formData),
    });
    // history.push(SIGNIN_ROUTE);
    console.log(result);
    // history.push(SIGNIN_ROUTE);
  };


  const colourStyles = {
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '20px',
      borderColor: '#959595',
    }),
    option: ({ isSelected }) => ({
      padding: '5px 10px',
      color: '#959595',
    }),
    container: (styles) => ({
      flex: '1 1 40%',
      position: 'relative',
    }),
    menu: () => ({
      position: 'absolute',
      backgroundColor: '#fff',
      width: '100%',
    }),
    input: (styles) => ({ ...styles }),
    placeholder: (styles) => ({ ...styles }),
    singleValue: (styles, { data }) => ({
      ...styles,
      color: '#959595',
      fontWeight: '600',
    }),
    valueContainer: (styles, { data }) => ({ ...styles, paddingLeft: '20px' }),
    indicatorSeparator: () => ({ display: 'none' }),
  };
  console.log(formData);
  return (
    <main className="sign-up">
      <form onSubmit={handleSubmit} className="form-wrapper">
        <h2 className="title">Sign Up</h2>
        <div className="col">
          <input
            onChange={handleForms}
            value={formData.firstName}
            name={'firstName'}
            type="text"
            placeholder="YOUR FIRST NAME..."
          />
        </div>
        <div className="col">
          <input
            onChange={handleForms}
            value={formData.email}
            name={'email'}
            type="email"
            placeholder="E-MAIL"
          />
        </div>
        <div className="col">
          <input
            onChange={handleForms}
            value={formData.lastName}
            name={'lastName'}
            type="text"
            placeholder="YOUR LAST NAME..."
          />
        </div>
        <div className="col">
          <input
            onChange={handleForms}
            value={formData.password}
            name={'password'}
            type="password"
            placeholder="PASSWORD"
          />
        </div>
        <div className="col z-3">
          <Select
            name="month"
            onChange={({ value }) => {
              setBirthMonth(value);
              setFormData({
                ...formData,
                ['birthdayStr']: `${birthDay}-${birthMonth}-${birthYear}`,
              });
            }}
            className="form-select form-select--month"
            defaultValue={months[1]}
            options={months}
            styles={colourStyles}
          />
          <Select
            name="day"
            onChange={({ value }) => {
              setBirthDay(value);
              setFormData({
                ...formData,
                ['birthdayStr']: `${birthDay}-${birthMonth}-${birthYear}`,
              });
            }}
            className="form-select form-select--day"
            defaultValue={days[1]}
            options={days}
            styles={colourStyles}
          />
          <Select
            name="year"
            onChange={({ value }) => {
              setBirthYear(value);
              setFormData({ ...formData, ['birthdayStr']: `${birthDay}-${birthMonth}-${value}` });
            }}
            className="form-select form-select--year"
            defaultValue={years[1]}
            options={years}
            styles={colourStyles}
          />
        </div>
        <div className="col">
          <input
            onChange={handleForms}
            value={formData.confirmPassword}
            name="confPassword"
            type="password"
            placeholder="CONFIRM PASSWORD"
          />
        </div>
        <div className="col">
          {' '}
          <Select
            name="countryId"
            onChange={({ value }) => setFormData({ ...formData, ['countryId']: value })}
            className="form-select form-select--month"
            defaultValue={countries[1]}
            options={countries}
            styles={colourStyles}
          />
        </div>
        <div className="col">
          <button type="submit" className="btn btn-sign-up">
            Sign Up
          </button>
        </div>
        <label className="notification">
          <input onChange={handleForms} checked={formData.noMail} type="checkbox" name="noMail" />
          <span>Receive notification</span>
        </label>
        <p className="terms">
          By clicking the button "Sign Up" you agree with{' '}
          <NavLink to={'/terms'}>Terms of Use Agreement</NavLink>
        </p>
      </form>
    </main>
  );
}

export default SignUp;
