export const authSelectOptions = {
  countries: [
    {
      value: 1,
      label: 'USA',
    },
    {
      value: 2,
      label: 'Ukraine',
    },
    {
      value: 3,
      label: 'Russia',
    },
    {
      value: 4,
      label: 'German',
    },
    {
      value: 5,
      label: 'UK',
    },
  ],
  months: [
    {
      value: 'january',
      label: 'January',
    },
    {
      value: 'february',
      label: 'February',
    },
    {
      value: 'march',
      label: 'March',
    },
    {
      value: 'april',
      label: 'April',
    },
    {
      value: 'may',
      label: 'May',
    },
    {
      value: 'june',
      label: 'June',
    },
    {
      value: 'july',
      label: 'July',
    },
    {
      value: 'august',
      label: 'August',
    },
    {
      value: 'september',
      label: 'September',
    },
    {
      value: 'october',
      label: 'October',
    },
    {
      value: 'november',
      label: 'November',
    },
    {
      value: 'december',
      label: 'December',
    },
  ],
  monthsNumber: [
    {
      value: '01',
      label: '01',
    },
    {
      value: '02',
      label: '02',
    },
    {
      value: '03',
      label: '03',
    },
    {
      value: '04',
      label: '04',
    },
    {
      value: '05',
      label: '05',
    },
    {
      value: '06',
      label: '06',
    },
    {
      value: '07',
      label: '07',
    },
    {
      value: '08',
      label: '08',
    },
    {
      value: '09',
      label: '09',
    },
    {
      value: '10',
      label: '10',
    },
    {
      value: '11',
      label: '11',
    },
    {
      value: '12',
      label: '12',
    },
  ],
  days: [
    {
      value: '1',
      label: '1',
    },
    {
      value: '2',
      label: '2',
    },
    {
      value: '3',
      label: '3',
    },
    {
      value: '4',
      label: '4',
    },
    {
      value: '5',
      label: '5',
    },
    {
      value: '6',
      label: '6',
    },
    {
      value: '7',
      label: '7',
    },
    {
      value: '8',
      label: '8',
    },
    {
      value: '9',
      label: '9',
    },
    {
      value: '10',
      label: '10',
    },
    {
      value: '11',
      label: '11',
    },
    {
      value: '12',
      label: '12',
    },
    {
      value: '13',
      label: '13',
    },
    {
      value: '14',
      label: '14',
    },
    {
      value: '15',
      label: '15',
    },
    {
      value: '16',
      label: '16',
    },
    {
      value: '17',
      label: '17',
    },
    {
      value: '18',
      label: '18',
    },
    {
      value: '19',
      label: '19',
    },
    {
      value: '20',
      label: '20',
    },
    {
      value: '21',
      label: '21',
    },
    {
      value: '22',
      label: '22',
    },
    {
      value: '23',
      label: '23',
    },
    {
      value: '24',
      label: '24',
    },
    {
      value: '25',
      label: '25',
    },
    {
      value: '26',
      label: '26',
    },
    {
      value: '27',
      label: '27',
    },
    {
      value: '28',
      label: '28',
    },
    {
      value: '29',
      label: '29',
    },
    {
      value: '30',
      label: '30',
    },
    {
      value: '31',
      label: '31',
    },
  ],
  years: [
    {
      value: '1963',
      label: '1963',
    },
    {
      value: '1964',
      label: '1964',
    },
    {
      value: '1965',
      label: '1965',
    },
    {
      value: '1966',
      label: '1966',
    },
    {
      value: '1967',
      label: '1967',
    },

    {
      value: '1968',
      label: '1968',
    },
    {
      value: '1969',
      label: '1969',
    },
    {
      value: '1970',
      label: '1970',
    },
    {
      value: '1971',
      label: '1971',
    },

    {
      value: '1972',
      label: '1972',
    },
    {
      value: '1973',
      label: '1973',
    },
    {
      value: '1974',
      label: '1974',
    },
    {
      value: '1975',
      label: '1975',
    },
    {
      value: '1976',
      label: '1976',
    },
    {
      value: '1977',
      label: '1977',
    },
    {
      value: '1978',
      label: '1978',
    },
    {
      value: '1979',
      label: '1979',
    },
    {
      value: '1980',
      label: '1980',
    },
    {
      value: '1981',
      label: '1981',
    },
    {
      value: '1982',
      label: '1982',
    },
    {
      value: '1983',
      label: '1983',
    },
    {
      value: '1984',
      label: '1984',
    },
    {
      value: '1985',
      label: '1985',
    },
    {
      value: '1986',
      label: '1986',
    },
    {
      value: '1987',
      label: '1987',
    },
    {
      value: '1988',
      label: '1988',
    },

    {
      value: '1989',
      label: '1989',
    },
    {
      value: '1990',
      label: '1990',
    },
    {
      value: '1991',
      label: '1991',
    },
    {
      value: '1992',
      label: '1992',
    },
    {
      value: '1993',
      label: '1993',
    },
    {
      value: '1994',
      label: '1994',
    },
    {
      value: '1995',
      label: '1995',
    },

    {
      value: '1996',
      label: '1996',
    },
    {
      value: '1997',
      label: '1997',
    },
    {
      value: '1998',
      label: '1998',
    },
    {
      value: '1999',
      label: '1999',
    },
    {
      value: '2000',
      label: '2000',
    },
    {
      value: '2001',
      label: '2001',
    },
    {
      value: '2002',
      label: '2002',
    },
  ],
  hairColor: [
    {
      value: 'blond',
      label: 'Blond',
    },
    {
      value: 'brown',
      label: 'Brown',
    },
    {
      value: 'black',
      label: 'Black',
    },
    {
      value: 'fair',
      label: 'Fair',
    },
    {
      value: 'chestnut',
      label: 'Chestnut',
    },
    {
      value: 'red',
      label: 'Red',
    },
  ],
  eyesColor: [
    {
      value: 'blue',
      label: 'Blue',
    },
    {
      value: 'green',
      label: 'Green',
    },
    {
      value: 'brown',
      label: 'Brown',
    },
    {
      value: 'hazel',
      label: 'Hazel',
    },
  ],
  drinking: [
    {
      value: 'never',
      label: 'Never',
    },
    {
      value: 'socially',
      label: 'Socially',
    },
    {
      value: 'occasionally',
      label: 'Occasionally',
    },
  ],
  smoking: [
    {
      value: 'yes',
      label: 'Yes',
    },
    {
      value: 'no',
      label: 'No',
    },
  ],
  education: [
    {
      value: 'student',
      label: 'Student',
    },
    {
      value: 'university degree',
      label: 'University degree',
    },
    {
      value: 'university',
      label: 'University (unfinished)',
    },
    {
      value: 'college',
      label: 'College',
    },
    {
      value: 'high school',
      label: 'High school',
    },
  ],
  lookingForAgeAt: [
    {
      value: '18',
      label: '18',
    },
    {
      value: '19',
      label: '19',
    },
    {
      value: '20',
      label: '20',
    },
    {
      value: '21',
      label: '21',
    },
    {
      value: '22',
      label: '22',
    },
    {
      value: '23',
      label: '23',
    },
    {
      value: '24',
      label: '24',
    },
    {
      value: '25',
      label: '25',
    },
    {
      value: '26',
      label: '26',
    },
    {
      value: '27',
      label: '27',
    },
    {
      value: '28',
      label: '28',
    },
    {
      value: '29',
      label: '29',
    },
    {
      value: '30',
      label: '30',
    },
    {
      value: '31',
      label: '31',
    },
    {
      value: '32',
      label: '32',
    },
    {
      value: '33',
      label: '33',
    },
    {
      value: '34',
      label: '34',
    },
    {
      value: '35',
      label: '35',
    },
    {
      value: '36',
      label: '36',
    },
    {
      value: '37',
      label: '37',
    },
    {
      value: '38',
      label: '38',
    },
    {
      value: '39',
      label: '39',
    },
    {
      value: '40',
      label: '40',
    },
    {
      value: '41',
      label: '41',
    },
    {
      value: '42',
      label: '42',
    },
    {
      value: '43',
      label: '43',
    },
    {
      value: '44',
      label: '44',
    },
    {
      value: '45',
      label: '45',
    },
    {
      value: '46',
      label: '46',
    },

    {
      value: '47',
      label: '47',
    },
    {
      value: '48',
      label: '48',
    },
    {
      value: '49',
      label: '49',
    },
    {
      value: '50',
      label: '50',
    },
    {
      value: '51',
      label: '51',
    },

    {
      value: '52',
      label: '52',
    },
    {
      value: '53',
      label: '53',
    },
    {
      value: '54',
      label: '54',
    },
    {
      value: '55',
      label: '55',
    },
    {
      value: '56',
      label: '56',
    },
    {
      value: '57',
      label: '57',
    },
    {
      value: '58',
      label: '58',
    },

    {
      value: '59',
      label: '59',
    },
    {
      value: '60',
      label: '60',
    },
    {
      value: '61',
      label: '61',
    },
    {
      value: '62',
      label: '62',
    },
    {
      value: '63',
      label: '63',
    },
    {
      value: '64',
      label: '64',
    },
    {
      value: '65',
      label: '65',
    },
    {
      value: '66',
      label: '66',
    },
    {
      value: '67',
      label: '67',
    },
    {
      value: '68',
      label: '68',
    },
    {
      value: '69',
      label: '69',
    },
    {
      value: '70',
      label: '70',
    },

    {
      value: '71',
      label: '71',
    },
    {
      value: '72',
      label: '72',
    },
    {
      value: '73',
      label: '73',
    },
    {
      value: '74',
      label: '74',
    },
    {
      value: '75',
      label: '75',
    },
    {
      value: '76',
      label: '76',
    },
    {
      value: '77',
      label: '77',
    },

    {
      value: '78',
      label: '78',
    },
    {
      value: '79',
      label: '79',
    },
    {
      value: '80',
      label: '80',
    },
  ],
  lookingForAgeTo: [
    {
      value: '18',
      label: '18',
    },
    {
      value: '19',
      label: '19',
    },
    {
      value: '20',
      label: '20',
    },
    {
      value: '21',
      label: '21',
    },
    {
      value: '22',
      label: '22',
    },
    {
      value: '23',
      label: '23',
    },
    {
      value: '24',
      label: '24',
    },
    {
      value: '25',
      label: '25',
    },
    {
      value: '26',
      label: '26',
    },
    {
      value: '27',
      label: '27',
    },
    {
      value: '28',
      label: '28',
    },
    {
      value: '29',
      label: '29',
    },
    {
      value: '30',
      label: '30',
    },
    {
      value: '31',
      label: '31',
    },
    {
      value: '32',
      label: '32',
    },
    {
      value: '33',
      label: '33',
    },
    {
      value: '34',
      label: '34',
    },
    {
      value: '35',
      label: '35',
    },
    {
      value: '36',
      label: '36',
    },
    {
      value: '37',
      label: '37',
    },
    {
      value: '38',
      label: '38',
    },
    {
      value: '39',
      label: '39',
    },
    {
      value: '40',
      label: '40',
    },
    {
      value: '41',
      label: '41',
    },
    {
      value: '42',
      label: '42',
    },
    {
      value: '43',
      label: '43',
    },
    {
      value: '44',
      label: '44',
    },
    {
      value: '45',
      label: '45',
    },
    {
      value: '46',
      label: '46',
    },

    {
      value: '47',
      label: '47',
    },
    {
      value: '48',
      label: '48',
    },
    {
      value: '49',
      label: '49',
    },
    {
      value: '50',
      label: '50',
    },
    {
      value: '51',
      label: '51',
    },

    {
      value: '52',
      label: '52',
    },
    {
      value: '53',
      label: '53',
    },
    {
      value: '54',
      label: '54',
    },
    {
      value: '55',
      label: '55',
    },
    {
      value: '56',
      label: '56',
    },
    {
      value: '57',
      label: '57',
    },
    {
      value: '58',
      label: '58',
    },

    {
      value: '59',
      label: '59',
    },
    {
      value: '60',
      label: '60',
    },
    {
      value: '61',
      label: '61',
    },
    {
      value: '62',
      label: '62',
    },
    {
      value: '63',
      label: '63',
    },
    {
      value: '64',
      label: '64',
    },
    {
      value: '65',
      label: '65',
    },
    {
      value: '66',
      label: '66',
    },
    {
      value: '67',
      label: '67',
    },
    {
      value: '68',
      label: '68',
    },
    {
      value: '69',
      label: '69',
    },
    {
      value: '70',
      label: '70',
    },

    {
      value: '71',
      label: '71',
    },
    {
      value: '72',
      label: '72',
    },
    {
      value: '73',
      label: '73',
    },
    {
      value: '74',
      label: '74',
    },
    {
      value: '75',
      label: '75',
    },
    {
      value: '76',
      label: '76',
    },
    {
      value: '77',
      label: '77',
    },

    {
      value: '78',
      label: '78',
    },
    {
      value: '79',
      label: '79',
    },
    {
      value: '80',
      label: '80',
    },
  ],
};
