import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Layout from './components/organism/Layout';
import Auth from './components/pages/Auth';
import ProfileState from './context/profile/ProfileState';

function App() {
  return (
    <Router className="App">
      <Switch>
        <ProfileState>
          <Layout>
            <Auth />
          </Layout>
        </ProfileState>
      </Switch>
    </Router>
  );
}

export default App;
